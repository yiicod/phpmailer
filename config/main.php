<?php

return [
    'components' => [
        'phpMailer' => [
            'class' => 'yiicod\phpmailer\components\PhpMailer',
            'host' => 'localhost', // if smtp
        ],
    ],
];
