PHP Mailer
==========

Config
------

```php
'aliases' => array(
    'vendor' => '...',
),
'components' => array(
    ...
    'phpMailer' => array(
        'class' => 'yiicod\phpmailer\components\PhpMailer',                            
        'smtpAuth' => false || true,
        'mailer' => 'smtp' || 'mail',
        'sender' => ...,
        'host' => 'localhost',
        'username' => '',
        'password' => '',
        'from' => '<email>|<name>',
        'altBody' => 'To view the message, please use an HTML compatible email viewer!',
    ),
    ...
)
```

Using
-----

```php
/**
 * Send mail
 * @param string $to Email to
 * @param string $subject Email subject
 * @param string Body email, html
 * @param string|Array From email
 * @param string Attach for email array('path' => 'file path', 'name' => 'file bname')
 */
Yii::app->phpMailer->send($to, $subject, $message, $from = '', $attachs) 
```  