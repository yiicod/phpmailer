<?php

namespace yiicod\phpmailer\interfaces;

/**
 * Mailer interface.
 */
interface MailerInterface
{
    /**
     * Send mail.
     *
     * @param string $to      Email to
     * @param string $subject Email subject
     * @param string Body email, html
     * @param string|array From email
     * @param string Attach for email array('path' => 'file path', 'name' => 'file bname')
     *
     * @author Orlov Alexey <aaorlov88@gmail.com>
     *
     * @version $Id$
     *
     * @since 1.0
     */
    public function send($to, $subject, $message, /* $header = '', */ $from = '', array $attachs = []);
}
