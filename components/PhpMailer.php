<?php

namespace yiicod\phpmailer\components;

use CApplicationComponent;
use CLogger;
use Exception;
use PHPMailer as PhpMailerLib;
use Yii;
use yiicod\phpmailer\interfaces\MailerInterface;

/**
 * Php Mailer component.
 *
 * @author Orlov Alexey <aaorlov88@gmail.com>
 *
 * @version $Id$
 *
 * @since 1.0
 */
class PhpMailer extends CApplicationComponent implements MailerInterface
{
    public $smtpAuth = false;
    public $mailer = 'mail';
    public $sender = '';
    public $host = 'localhost';
    public $username = '';
    public $password = '';
    public $debug = 0;
    public $charSet = 'utf-8';
    public $from = 'noreply@outbox.com | Site';
    public $altBody = 'To view the message, please use an HTML compatible email viewer!';

    /**
     * PhpMailerLib.
     *
     * @var PhpMailerLib
     */
    private $phpMailer = null;

    /**
     * @param PhpMailerLib|null $phpMailer
     */
    public function setPhpMailer($phpMailer)
    {
        $this->phpMailer = $phpMailer;
    }

    /**
     * @return PhpMailerLib
     */
    public function getPhpMailer()
    {
        return $this->phpMailer === null ? new PhpMailerLib() : $this->phpMailer;
    }

    /**
     * Send mail.
     *
     * @param string $to Email to
     * @param string $subject Email subject
     * @param string $message
     * @param string $from
     * @param array $attachs
     * @return bool
     */
    public function send($to, $subject, $message, $from = '', array $attachs = [])
    {
        try {
            $this->phpMailer = $this->getPhpMailer();

            $this->phpMailer->CharSet = $this->charSet;
            //Set mailer "smtp" or "mail"
            $this->phpMailer->Mailer = $this->mailer;
            //Set Host 
            if (null !== $this->host) {
                $this->phpMailer->Host = $this->host;
            }
            //If smtp auth true and mailer smtp then auth
            if ($this->smtpAuth && $this->mailer == 'smtp') {
                $this->phpMailer->SMTPDebug = $this->debug;
                $this->phpMailer->SMTPAuth = $this->smtpAuth;
                $this->phpMailer->Username = $this->username;
                $this->phpMailer->Password = $this->password;
            }
            //Set sender
            $this->phpMailer->Sender = $this->sender;
            //Set from

            if (mb_strpos($from, '|')) {
                $this->phpMailer->SetFrom(trim(mb_substr($from, 0, mb_strpos($from, '|') - 1)), trim(mb_substr($from, mb_strpos($from, '|') + 1)));
            } elseif (!empty($from)) {
                $this->phpMailer->SetFrom($from, $from);
            } elseif (mb_strpos($this->from, '|')) {
                $this->phpMailer->SetFrom(trim(mb_substr($this->from, 0, mb_strpos($this->from, '|') - 1)), trim(mb_substr($this->from, mb_strpos($this->from, '|') + 1)));
            } elseif (empty($this->from)) {
                $this->phpMailer->SetFrom($this->from, $this->from);
            }

            //Set subject
            $this->phpMailer->Subject = $subject;
            //Set alt text
            $this->phpMailer->AltBody = $this->altBody;
            //Set html message
            $this->phpMailer->MsgHTML($message);
            if (is_array($attachs)) {
                foreach ($attachs as $attach) {
                    $this->phpMailer->AddAttachment($attach['path'], $attach['name']);
                }
            }

            //var_dump($this->phpMailer);die;
            ob_start();
            //Add address and send
            if (is_array($to)) {
                foreach ($to as $email) {
                    $this->phpMailer->AddAddress($email);
                    $this->phpMailer->Send();
                    $this->phpMailer->ClearAddresses();
                }
            } else {
                $this->phpMailer->AddAddress($to);
                $this->phpMailer->Send();
            }
            echo $this->phpMailer->ErrorInfo;
            $errors = ob_get_contents();
            ob_get_clean();
            $isSend = !$this->phpMailer->IsError() && empty($this->phpMailer->ErrorInfo);
            if (!$isSend) {
                Yii::log('CPhpMailer: ' . $errors, CLogger::LEVEL_ERROR, 'system.phpmailer');
            }
        } catch (Exception $e) {
            Yii::log('CPhpMailer: ' . $e->getMessage(), CLogger::LEVEL_ERROR, 'system.phpmailer');
            $isSend = false;
        }

        $this->setPhpMailer(null);

        return $isSend;
    }
}
